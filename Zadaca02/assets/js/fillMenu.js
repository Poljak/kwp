
class Action{
    constructor(name, image, link){
        this.name = name;
        this.image = image;
        this.link = link;
    }
    
}

let inventuraImg = { name : "News.svg" , alt: " Kalkulator" };
let inventarImg = { name : "Box.svg" , alt: " Box" };
let prostorijeImg = { name : "Classroom.svg" , alt: "Ploca " };
let zaposleniciImg = { name : "Contacts.svg" , alt: " Lik" };
let administracijaImg = { name : "Services.svg" , alt: "Tocak" };

let inventura = new Action("Inventura",inventuraImg, "#Inventura");
let inventar = new Action("Inventar", inventarImg , "#Inventar ");
let prostorije = new Action("Prostorije", prostorijeImg , "#Prostorije");
let zaposlenici = new Action("Zaposlenici", zaposleniciImg, "#Zaposlenici ");
let administracija = new Action("Administracija", administracijaImg, "#Administracija ");


const actions = [
    inventura,
    inventar,
    prostorije,
    zaposlenici,
    administracija
];


function fillMenu(actions){

    for(action of actions){

        let myUl = document.getElementById("listOfMenuItems");
        let newLi = document.createElement('li');
        let newA = document.createElement('a');
        let img = document.createElement('img');
        img.setAttribute ('src', 'assets/img/' + action.image.name);
        img.setAttribute('alt', action.image.alt);
        img.setAttribute('height','20');
        img.setAttribute('width','20');
        newA.appendChild(img);
        newA.appendChild(document.createTextNode(action.name));
        newLi.appendChild(newA);
        myUl.appendChild(newLi);

    } 
}
fillMenu(actions);