

class Room{
    constructor(name, itemsCount){
        this.name = name;
        this.itemsCount = itemsCount;

    }

}

let predavaonica1 = new Room ( "Predavaonica 1", "50");
let predavaonica2 = new Room ( "Predavaonica 2", "50");
let predavaonica3 = new Room ( "Predavaonica 3", "50");
let predavaonica4 = new Room ( "Predavaonica 4", "50");
let predavaonica5 = new Room ( "Predavaonica 5", "50");
let predavaonica6 = new Room ( "Predavaonica 6", "50");
let portirnica = new Room ( "Portirnica", "50");
let referada = new Room ( "Referada", "50");

const rooms = [
    predavaonica1,
    predavaonica2,
    predavaonica3,
    predavaonica4,
    predavaonica5,
    predavaonica6,
    portirnica,
    referada

];


function fillRooms(rooms){

    for(room of rooms){

        let myUl = document.getElementById("pred");
        let newLi = document.createElement('li');
        let newA = document.createElement('a');
        let newP1 = document.createElement('p');
        let newP2 = document.createElement('p');

        newP1.setAttribute('class', 'head_room');
        newP1.textContent = room.name;
        newP2.innerHTML = "Broj predmeta: " + room.itemsCount;
        newA.appendChild(newP1);
        newA.appendChild(newP2);
        newLi.appendChild(newA);
        myUl.appendChild(newLi);
    }

}
fillRooms(rooms);

/*
ul id ="pred"
  <li>
    ..
  <li>
*/

/*
Popratni link w3s 
https://www.w3schools.com/howto/howto_js_sort_table.asp
*/

var sorted = false;
function sortList(){

  sorted = !sorted;
  if(!sorted){
    document.getElementById("arrow").className = "arrowRotateUp";
  }
  if(sorted){
    document.getElementById("arrow").className = "arrowRotateDown";
  }

  var a, b, k, list, switching, shouldSwitch, dir, switchcount = 0;

  //uzmi sve li iz ul-a s id-jem pred
  list = document.getElementById("pred");
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc";

  /* Make a loop that will continue until
  no switching has been done: */
  while(switching){
    // Start by saying: no switching is done:
    switching = false;

    // svi <p> u kojima je nazvi prostorije
    x = list.getElementsByClassName('head_room');
    //  <li> od svakog <p>
    y = list.getElementsByTagName('li');

    for(k = 0; k < (x.length - 1); k++){
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if(dir == "asc"){
        if(x[k].innerHTML.toLowerCase() > x[k + 1].innerHTML.toLowerCase()){
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }else if(dir == "desc"){
        if(x[k].innerHTML.toLowerCase() < x[k + 1].innerHTML.toLowerCase()){
          // If so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if(shouldSwitch){
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */

      // zamijeni cijeli <li> sa prethodnim <li>
      y[k].parentNode.insertBefore(y[k + 1], y[k]); 
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    }else{
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if(switchcount == 0 && dir == "asc"){
        dir = "desc";
        switching = true;
      }
    }
  }
}
