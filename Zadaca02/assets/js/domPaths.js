


let whenClick = 0;
let node1;
let node2;

window.addEventListener('click', function(event){
    //Kada stisnemo na stranicu uvecavamo brojac i pravimo if,else 
    whenClick++;
    //Stavljamo target na prvi nod(nod koji smo stisnili)
    if(whenClick === 1){
      node1 = event.target;
    }
    //Ako je whenClick == 2 imamo dva noda te cemo izmeriti path
    if(whenClick == 2){
      node2 = event.target;
      // BITNO stavljamo === zato sto nam treba isti type, ako stavimo == dobiti cemo different type
      if(node1 === node2){
        console.log(node1.tagName.toLowerCase());
      }else{
        //path postaje array koji nece sadrzavati samo tagName od elementa u arrayu
        let path = min_path(node1, node2);
        //Koristimo filter tako da dobijemo samo tagName, (xy=document.getelemid), xy da je hrpa stvari ...
        path = filterTags(path);
        //Element mapiramo samo s ->
        console.log(path.join(' -> '));
      }
      // restart
      whenClick = 0;
      node1 = 0;
      node2 = 0;
    }
});

function filterTags(arry){
  let tagList = [];
  for(let i in arry){
    let doc = arry[i];
    tagList.push(doc.tagName.toLowerCase());
  }
  return tagList;
}



//Algoritam kopiran s stranice
// https://stackoverflow.com/questions/33368604/get-dom-path-from-a-node-to-another
function min_path(node1, node2) {

    let node_1_ancestors = get_ancestors(node1);
    let node_2_ancestors = get_ancestors(node2);

    let divergent_index = 0;
    while(node_1_ancestors[divergent_index] === node_2_ancestors[divergent_index]) {
        divergent_index++;
    }

    let path = [];
    for(let i = node_1_ancestors.length - 1; i >= divergent_index - 1; i--) {
        path.push(node_1_ancestors[i]);
    }
    for(let i = divergent_index; i < node_2_ancestors.length; i++) {
        path.push(node_2_ancestors[i]);
    }

    return path;
}

function get_ancestors(node) {
    let ancestors = [node];
    while(ancestors[0] !== null) {
        ancestors.unshift(ancestors[0].parentElement);
    }
    return ancestors;
}
