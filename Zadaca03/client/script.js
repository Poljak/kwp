let getItems = (callb) => {
    $.ajax("/item")
        .done(data => callb(data));
}

let addItem = (name, room, quantity, inUsage, callb) => {
    $.ajax("/item", {data: { name, room, quantity, inUsage }, method: "POST"})
        .done(data => callb(data));
}


let updateItem = (id, inUsage) => {
    $.ajax({
        url: `/item/${id}`,
        type: 'PUT',
        data: { inUsage },
        success: function(data) {
            fillTable(data.items);
        }
    });
}

let fillTable = (items) => {
    let tbody = $("#inventory-table > tbody")
    tbody.empty();
   
    for(let i = 0; i < items.length; i++) {
        let item = items[i];
        if (!tbody.empty()){
            tbody.shift();
        }
        
        inUsageStr = item.inUsage 
        
        tbody.append(
            `<tr>
                <td>
                <h3>${item.name}</h3> 
                <p>${item.room}</p>     
                </td>
            </tr>
            `
        )       
    }
}

///
let fillList = (messages) => {
    let ul = $("#DisplayMessages")
    ul.empty();
    let len = 0;
    if(messages.length < 6){
        len = messages.length;
        $("#hideMe").hide(); 
    }else{
        len = 5;
        $("#hideMe").show(); 
    }
    for(let i = 0; i < len; i++) {
        let message = messages[i];
        ul.append(`<li><p>${message}</p></li>`)
      
    }    
}

let NewMessage = (message, callb) => {
    $.ajax("/newmessage", {data: {message}, method: "POST"})
        .done(data => callb(data));
}

let getMessages = (callb) => {
    $.ajax("/message")
        .done(data => callb(data));
}

$(() => {
    // čim se dokument učita, dohvati item zapise te ih dodaj na stranicu pomocu metode fillTable
    getItems((data) => {
        console.log(data)
        fillTable(data.items);
    });

    $("#inventory-form").submit((event) => {
        let name = event.target[0].value;
        let room = event.target[1].value;
        let quantity = event.target[2].value;
        let inUsage = event.target[3].checked;

        event.target[0].value = "";
        event.target[1].value = "";
        event.target[2].value = "";
        event.target[3].checked = false;
        event.preventDefault();
        addItem(name, room, quantity, inUsage, (data) => {
            //fillTable(data.items)
            $(location).attr('href', 'profil.html');
            fillTable(data.items);
            
        });
    }); 
    
    getMessages((data) => {
        fillList(data.tosend);
    });

    $("#TypeMessage").keyup((event) => {  
        let message = $("#TypeMessage").val();
        let len = message.length;
        if(len < 500){
            $("#CharNum").text((500-len) + ' characters left');
            document.getElementById("SendMessage").disabled = false;
        }else{
            $("#CharNum").text('Message too long - Unable to submit');
            document.getElementById("SendMessage").disabled = true;
        }
    })

    $("#MessageForm").submit((event) => {
        let message = event.target[0].value;
        event.preventDefault();

        NewMessage(message, (data) => {
            fillList(data.tosend);
            
            
        });
    });

    $(document).on('change',"#CoverFile", (event) => {
        var files = $("#CoverFile").get(0).files;
        console.log(files);
    });
});