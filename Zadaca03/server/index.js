const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const morgan = require('morgan')

let app = express();
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, '../client')));

class Item {
    constructor(id, name, room, quantity, inUsage) {
        this.id = id;
        this.name = name;
        this.room = room;
        this.quantity = quantity;
        this.inUsage = inUsage;
    }
}

let items = [
    
];

let messages = [
    
];

let currentID = 0;

app.get('/item', (req, res) => {
    res.json({ items });
});

app.get('/message', (req, res) => {
    let tosend = [];
    if(messages.length<6){
        tosend = messages;
    }else{ 
        for(let i=0; i<6; i++){
            tosend[i] = messages[i];
        }
    }

    res.json({ tosend });
});
app.get('/allmessage', (req, res) => {
    res.json({ messages });
});
app.post('/newmessage', (req, res) => {
    let message = req.body.message;

    messages.unshift(message);

    let tosend = [];
    if(messages.length<6){
        tosend = messages;
    }else{ 
        for(let i=0; i<6; i++){
            tosend[i] = messages[i];
        }
    }

    res.json({ tosend });
});


app.post('/item', (req, res) => {

    let name = req.body.name;
    let room = req.body.room;
    let quantity = parseInt(req.body.quantity);
    let inUsage = req.body.inUsage == 'true';

    let item = new Item(++currentID, name, room, quantity, inUsage);

    items.push(item);

    res.json({ items });
});




app.delete('/item/:id', (req, res) => {
    let id = parseInt(req.params.id);
    items = items.filter(item => item.id !== id); 
    res.json({ items })
});


app.put('/item/:id', (req, res) => {
    
    let id = parseInt(req.params.id);
    let inUsage = req.body.inUsage == 'true';

    for(let i = 0; i < items.length; ++i)
        if(id == items[i].id)
            items[i].inUsage = inUsage;

    res.json({ items });
});

app.use('**', (req, res) => {
    res.sendFile(path.join(__dirname, '../client/index.html'));
});


app.listen(3000, () => {
    console.log('Listening on port ' + 3000 + '!')
});